const Joi = require('@hapi/joi');
const bcrypt = require('bcrypt');
const { uploadUserImage, hashPassword } = require('./helpers');
const User = require('./model');
const { CustomError } = require('../../configs/exceptions');

Joi.objectId = require('joi-objectid')(Joi);

const JOI_FIELDS = {
  email: Joi.string().email().max(150),
  password: Joi.string().min(8).max(150),
  token: Joi.string().regex(/^[A-Za-z0-9-_]+\.[A-Za-z0-9-_]+\.[A-Za-z0-9-_.+/=]*$/).required(),
  firstName: Joi.string().max(50).allow(''),
  lastName: Joi.string().max(150).allow(''),
  profilePictureString: Joi.string().base64().allow(''),
  profilePictureFile: Joi.object(),
};

const registerValidator = async (ctx, next) => {
  const schema = Joi.object(
  );
  ctx.request.body = await schema.validateAsync(ctx.request.body, { abortEarly: false });
  await next();
};

const loginValidator = async (ctx, next) => {
  const schema = Joi.object(
    {
      email: JOI_FIELDS.email.required(),
      password: JOI_FIELDS.password.min(0).required(),
    },
  );
  await schema.validateAsync(ctx.request.body, { abortEarly: false });
  await next();
};

const emailValidator = async (ctx, next) => {
  const schema = Joi.object(
    {
      email: JOI_FIELDS.email.required(),
    },
  );
  await schema.validateAsync(ctx.request.body, { abortEarly: false });
  await next();
};

const emailVerifyValidator = async (ctx, next) => {
  const schema = Joi.object(
    {
      token: JOI_FIELDS.token,
    },
  );
  await schema.validateAsync(ctx.request.body, { abortEarly: false });
  await next();
};

const resetPasswordValidator = async (ctx, next) => {
  const schema = Joi.object(
    {
      token: JOI_FIELDS.token,
      password: JOI_FIELDS.password.required(),
    },
  );
  await schema.validateAsync(ctx.request.body, { abortEarly: false });
  await next();
};

const userProfileValidator = async (ctx, next) => {
  const checkPassword = async (password) => {
    let user;
    if (ctx.state.isAdmin) {
      user = await User.findOne({ _id: ctx.params.id }, 'password email');
    } else {
      user = await User.findOne({ _id: ctx.state.user }, 'password email');
      const match = await bcrypt.compare(password, user.password);
      if (!match) {
        throw new CustomError('Incorrect password entered');
      }
    }
    if (ctx.request.body.email.trim().toLowerCase() !== user.email) {
      ctx.request.body.emailVerified = false;
    }
  };
  const schemaObject = {
    firstName: JOI_FIELDS.firstName,
    lastName: JOI_FIELDS.lastName,
    profilePicture: JOI_FIELDS.profilePictureString,
    email: JOI_FIELDS.email,
    password: JOI_FIELDS.password.when('email', { then: Joi.required() }),
  };
  if (ctx.state.isAdmin) {
    schemaObject.isActive = Joi.boolean();
  }
  const schema = Joi.object(
    schemaObject,
  );
  await schema.validateAsync(ctx.request.body, { abortEarly: false });
  if (ctx.request.body.password) {
    await checkPassword(ctx.request.body.password);
    delete ctx.request.body.password;
  }

  if (!ctx.request.body.profilePicture && ctx.request.file) {
    const fileSchema = Joi.object(
      {
        profilePicture: JOI_FIELDS.profilePictureFile,
      },
    );
    await fileSchema.validateAsync({ profilePicture: ctx.request.file });
    ctx.request.body.profilePicture = ctx.request.file;
  }
  if (ctx.request.body.profilePicture !== undefined) {
    const imagePath = await uploadUserImage(ctx.request.body.profilePicture, ctx.state.user);
    ctx.request.body.profilePicture = imagePath;
  }
  await next();
};

const changePasswordValidator = async (ctx, next) => {
  const schema = Joi.object(
    {
      oldPassword: JOI_FIELDS.password.required().label('Old password'),
      newPassword: JOI_FIELDS.password.required().label('New password'),
    },
  );
  await schema.validateAsync(ctx.request.body, { abortEarly: false });
  await next();
};

const listUserValidator = async (ctx, next) => {
  const schema = Joi.object(
    {
      for: Joi.string().valid(
        'productYintro', 'productPurchases', 'productViews', 'productRegistration', 'yintroView',
        'yintroPurchase', 'yintroRegistered',
      ),
      product: Joi.objectId(),
      yintro: Joi.objectId(),
      search: Joi.string().max(100),
    },
  );
  ctx.request.query = await schema.validateAsync(ctx.request.query, { abortEarly: true });
  if (!ctx.request.query.for && !ctx.state.isAdmin) {
    throw new CustomError('Invalid request');
  }
  if (ctx.request.query.search) {
    ctx.request.query.$or = [
      { firstName: { $regex: ctx.request.query.search, $options: 'i' } },
      { lastName: { $regex: ctx.request.query.search, $options: 'i' } },
      { email: { $regex: ctx.request.query.search, $options: 'i' } },
    ];
    delete ctx.request.query.search;
  }
  await next();
};

const updateBankAccValidator = async (ctx, next) => {
  const schema = Joi.object(
    {
      bankAccountName: Joi.string().max(250).allow('').required(),
      bankAccountNumber: Joi.string().max(100).allow('').required(),
      bsb: Joi.string().max(100).allow('').required(),
    },
  );
  ctx.request.body = await schema.validateAsync(ctx.request.body, { abortEarly: false });
  await next();
};

module.exports = {
  registerValidator,
  emailValidator,
  emailVerifyValidator,
  resetPasswordValidator,
  userProfileValidator,
  changePasswordValidator,
  loginValidator,
  listUserValidator,
  updateBankAccValidator,
};
