
const mongoose = require('mongoose');
const User = require('./model');
const { getJWTToken, getJWTPayload } = require('../../utils/auth');
const {
  REGISTERATION_SUCCESS, INVALID_CRED, LOGIN_SUCCESS, VERIFICATION_MAIL_SENT,
  EMAIL_VERIFIED, RESET_PASSWORD_MAIL_SENT, PASSWORD_UPDATED, UPDATED_SUCCESS,
} = require('../../configs/strings');
const { CustomError } = require('../../configs/exceptions');

const { consoleLogger } = require('../../configs/logger');


const register = async (ctx) => {
  try{
    const user = await User.create(ctx.request.body);
    const token = getJWTToken(
    {
      user: user.idlvr_id
    },
    '12000s',
  );

    ctx.body = {
      data: {
        user: user.toJSON(),
        token:token
      },
      message: REGISTERATION_SUCCESS,
    };}
    catch(err){
      consoleLogger.error(err);
  };
};



const updateProfile = async (ctx) => {
  let user = await User.findOne({idlvr_id:ctx.state.user}).sort('-createdAt');
  user = Object.assign(user, ctx.request.body);
  user.save();
  ctx.body = {
    data: user.toJSON(),
    message: UPDATED_SUCCESS,
  };
};

const userDetail = async (ctx) => {
  const user = await User.findOne({idlvr_id:ctx.params.id}).sort('-createdAt');
  ctx.body = {
    data: user.toJSON(),
  };
};


module.exports = {
  register,
  updateProfile,
  userDetail,
};
