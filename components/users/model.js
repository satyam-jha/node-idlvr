/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */
const mongoose = require('mongoose');


const userSchema = new mongoose.Schema(
  {
    idlvr_id: {
      type:String,
      unique: true
    },

    last_location: {
      type: {
				type: String,
				default: "Point",
			},
			coordinates: [Number],
    },
    usertype: {
      type: String,
      default:"D",
      enum: ["D", "C"]
    },
  },
  {
    id: false,
    timestamps: true,
  },
);


userSchema.set('toJSON', {
  virtuals: true,
  transform: (doc, ret) => {
    delete ret.__v;
    return ret;
  },
});

const User = mongoose.model('User', userSchema);

module.exports = User;
