const Router = require('@koa/router');
const multer = require('@koa/multer');
const {
  register, updateProfile, userDetail,
} = require('./controller');
const {
  registerValidator,
} = require('./validators');
const { isAuthentication } = require('../../utils/auth');


const router = new Router();

router.post('/register', registerValidator, register);
router.get('/profile', isAuthentication, updateProfile);
router.get('/:id', isAuthentication, userDetail);
router.patch('/:id', isAuthentication, updateProfile);
module.exports = router;
