const bcrypt = require('bcrypt');
const ejs = require('ejs');
const path = require('path');
const uuid = require('uuid');
const { PASSWORD_MIN_LENGTH } = require('../../configs/strings');
const { CustomError } = require('../../configs/exceptions');
const { getJWTToken } = require('../../utils/auth');
const { sendSESMail } = require('../../utils/emails');
const { uploadFile } = require('../../utils/s3');

const hashPassword = async (password) => {
  if (password.length < 8) {
    throw new CustomError(PASSWORD_MIN_LENGTH);
  }
  const hashedPassword = await bcrypt.hash(password, 10);
  return hashedPassword;
};

const emailVerificationMail = async (customCtx) => {
  const token = getJWTToken({
    user: customCtx.userId,
    emailVerify: true,
  });
  const link = `${process.env.WEB_URL}/verify-email/${token}`;
  const renderedString = await ejs.renderFile(path.join(__dirname, '/templates/welcome.ejs'), { link });
  sendSESMail([customCtx.email], renderedString, 'Email verification');
};

const sendResetPasswordMail = async (customCtx) => {
  const token = getJWTToken({
    user: customCtx.userId,
    resetPassword: true,
  });
  const link = `${process.env.WEB_URL}/reset-password/${token}`;
  const renderedString = await ejs.renderFile(path.join(__dirname, '/templates/reset-password.ejs'), { link });
  sendSESMail([customCtx.email], renderedString, 'Reset Password');
};

const uploadUserImage = async (value, userId) => {
  if (!value) {
    return null;
  }
  const filename = value.filename || `${uuid.v4().replace(/-/g, '')}.png`;
  const key = `users/${userId}/${filename}`;
  uploadFile(value, key, true);
  return key;
};

module.exports = {
  hashPassword,
  emailVerificationMail,
  sendResetPasswordMail,
  uploadUserImage,
};
