
const mongoose = require('mongoose');
const ChatRoom = require('../chatroom/model')

const MessageSchema = new mongoose.Schema(
  {
    sender:{
        type:String,
        required:true
    },
    message:{
        type:String,
        required:true
    },
    room:{
      type:mongoose.Types.ObjectId,
      ref:'ChatRoom'
    },
    is_read:{
      type:Boolean,
      default:false
    } 
  },
  {
    id: false,
    timestamps: true,
  },
);


MessageSchema.set('toJSON', {
  virtuals: true,
  transform: (doc, ret) => {
    delete ret.__v;
    return ret;
  },
});

const Chat = mongoose.model('Chat', MessageSchema);

module.exports = Chat;
