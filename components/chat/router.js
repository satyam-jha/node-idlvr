const Router = require('@koa/router');
const multer = require('@koa/multer');
const {CreateMessage, getallMessage} = require('./controller');

const { isAuthentication } = require('../../utils/auth');


const router = new Router();

router.post('/', CreateMessage);
router.get('/', getallMessage);
// router.get('/:id', isAuthentication, getRoomDetails);
module.exports = router;
