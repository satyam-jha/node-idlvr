
const mongoose = require('mongoose');
const Chat = require('./model');
const { getJWTToken, getJWTPayload } = require('../../utils/auth');
const {
  REGISTERATION_SUCCESS, INVALID_CRED, LOGIN_SUCCESS, VERIFICATION_MAIL_SENT,
  EMAIL_VERIFIED, RESET_PASSWORD_MAIL_SENT, ROOM_CREATED, MESSAGE_CREATED,
} = require('../../configs/strings');
const { CustomError } = require('../../configs/exceptions');

const { consoleLogger } = require('../../configs/logger');


const CreateMessage = async (ctx) => {
  try{
    const data = ctx.request.body;
    const message = await Chat.create({sender:ctx.state.user, message:data.message, room:data.room});

    ctx.body = {
      data: {
        message: message.toJSON(),
      },
      message: MESSAGE_CREATED,
    };}
    catch(err){
      consoleLogger.error(err);
  };
};


const getallMessage = async (ctx) => {
    try{
    const chats = await Chat.find({room:ctx.request.query.room}).sort('-createdAt').populate('room');
    ctx.body = {
      data:{
        messages: chats,
      }
    };}
    catch(err){
        consoleLogger.error(err)
    };
};

module.exports = {
    CreateMessage,
    getallMessage,
};
