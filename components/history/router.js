const Router = require('@koa/router');
const {
  updatelocation,
} = require('./controller');
const { isAuthentication } = require('../../utils/auth');

const router = new Router();
router.post('/history', isAuthentication, updatelocation);

module.exports = router;
