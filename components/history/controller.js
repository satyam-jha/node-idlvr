const mongoose = require('mongoose');
const History = require('./model');
const User = require('../users/model')
const { getJWTToken, getJWTPayload } = require('../../utils/auth');
const {
  REGISTERATION_SUCCESS, INVALID_CRED, LOGIN_SUCCESS, VERIFICATION_MAIL_SENT,
  EMAIL_VERIFIED, RESET_PASSWORD_MAIL_SENT, PASSWORD_UPDATED, UPDATED_SUCCESS,
} = require('../../configs/strings');
const { CustomError } = require('../../configs/exceptions');

const { consoleLogger } = require('../../configs/logger');


const updatelocation = async (ctx) => {
    try{
        let user = await User.findOne({idlvr_id:ctx.state.user}).sort('-createdAt');
        user = Object.assign(user, {"last_location": ctx.request.body.location});
        user.save();
      const history = await History.create({"user":user._id, "location": ctx.request.body.location});
      ctx.body = {
        data: {
          history: history.toJSON(),
        },
        message: UPDATED_SUCCESS,
      };}
      catch(err){
        consoleLogger.error(err);
    };
  };


  module.exports = {
    updatelocation
  };
  