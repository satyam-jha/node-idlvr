
const mongoose = require('mongoose');


const locationSchema = new mongoose.Schema(
  {
    location: {
        type: {
				type: String,
				default: "Point",
			},
	    coordinates: [Number],
    },
    user:{
        type:mongoose.Types.ObjectId,
        ref:'User'
    }
  },
  {
    id: false,
    timestamps: true,
  },
);


locationSchema.set('toJSON', {
  virtuals: true,
  transform: (doc, ret) => {
    delete ret.__v;
    return ret;
  },
});

const History = mongoose.model('History', locationSchema);

module.exports = History;
