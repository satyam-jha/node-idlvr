
const mongoose = require('mongoose');


const ChatRoomSchema = new mongoose.Schema(
  {
    users:{
        type:Array,
        required:true
    },
    last_message:{
        type:String,
    }
  },
  {
    id: false,
    timestamps: true,
  },
);


ChatRoomSchema.set('toJSON', {
  virtuals: true,
  transform: (doc, ret) => {
    delete ret.__v;
    return ret;
  },
});

const ChatRoom = mongoose.model('ChatRoom', ChatRoomSchema);

module.exports = ChatRoom;
