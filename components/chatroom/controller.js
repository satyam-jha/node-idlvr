
const mongoose = require('mongoose');
const ChatRoom = require('./model');
const { getJWTToken, getJWTPayload } = require('../../utils/auth');
const {
  REGISTERATION_SUCCESS, INVALID_CRED, LOGIN_SUCCESS, VERIFICATION_MAIL_SENT,
  EMAIL_VERIFIED, RESET_PASSWORD_MAIL_SENT, ROOM_CREATED, UPDATED_SUCCESS,
} = require('../../configs/strings');
const { CustomError } = require('../../configs/exceptions');

const { consoleLogger } = require('../../configs/logger');


const CreateRoom = async (ctx) => {
  try{
    const data = ctx.request.body;
    const room = await ChatRoom.create({users:data.users});

    ctx.body = {
      data: {
        ChatRoom: room.toJSON(),
      },
      message: ROOM_CREATED,
    };}
    catch(err){
      consoleLogger.error(err);
  };
};


const getallRoom = async (ctx) => {
    try{
    const rooms = await ChatRoom.find({users:{"$in": [ctx.request.query.id]}}).sort('-createdAt');
    ctx.body = {
        data: rooms,
    };}
    catch(err){
        consoleLogger.error(err)
    };
};

const getRoomDetails = async (ctx) => {
    try{
    const rooms = await ChatRoom.findById(ctx.params.id);
    ctx.body = {
        data: rooms,
    };}
    catch(err){
        consoleLogger.error(err)
    };
};


module.exports = {
    CreateRoom,
    getRoomDetails,
    getallRoom,
};
