const Router = require('@koa/router');
const multer = require('@koa/multer');
const {CreateRoom, getallRoom, getRoomDetails} = require('./controller');

const { isAuthentication } = require('../../utils/auth');


const router = new Router();

router.post('/',  isAuthentication, CreateRoom);
router.get('/', isAuthentication, getallRoom);
router.get('/:id', isAuthentication, getRoomDetails);
module.exports = router;
