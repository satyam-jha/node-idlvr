const AWS = require('aws-sdk');
const fs = require('fs').promises;
const { consoleLogger } = require('../configs/logger');

const uploadFile = async (file, key, publicAcl = false) => {
  try {
    const s3 = new AWS.S3({ apiVersion: '2006-03-01' });
    let fileData;
    if (typeof file === 'string') {
      fileData = Buffer.from(file, 'base64');
    } else {
      fileData = await fs.readFile(file.path);
    }
    const s3Options = {
      Bucket: process.env.S3_BUCKET_NAME,
      Key: key,
      Body: fileData,
      ContentType: file.mimetype || 'image/png',
    };
    if (publicAcl) {
      s3Options.ACL = 'public-read';
    }
    await s3.upload(s3Options).promise();
    return key;
  } catch (err) {
    consoleLogger.error({
      ref: 'Error while uploading file on s3',
      error: err.message,
      stack: err.stack,
    });
    return err.message;
  }
};

const deleteS3Object = async (key) => {
  try {
    const objectParams = { Bucket: process.env.S3_BUCKET_NAME, Key: key };
    const response = await new AWS.S3({ apiVersion: '2006-03-01' }).deleteObject(objectParams).promise();
    consoleLogger.info({
      ref: 'Delete s3 object response',
      key,
      response,
    });
  } catch (err) {
    consoleLogger.error({
      ref: 'Error while deleting file on s3',
      error: err.message,
      stack: err.stack,
    });
  }
};

const getS3ObjectSignedUrl = (key) => {
  let url = key;
  try {
    const s3 = new AWS.S3();
    const params = { Bucket: process.env.S3_BUCKET_NAME, Key: key };
    url = s3.getSignedUrl('getObject', params);
  } catch (err) {
    consoleLogger.error({
      ref: 'Error while getting file url from s3',
      error: err.message,
      stack: err.stack,
    });
  }
  return url;
};

const getS3ObjectPublicUrl = (key) => (
  `https://${process.env.S3_BUCKET_NAME}.s3.${process.env.AWS_REGION}.amazonaws.com/${key}`
);

module.exports = {
  uploadFile,
  deleteS3Object,
  getS3ObjectSignedUrl,
  getS3ObjectPublicUrl,
};
