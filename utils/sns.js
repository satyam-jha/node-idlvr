const AWS = require('aws-sdk');
const { consoleLogger } = require('../configs/logger');

const sendSNSMessage = async (params) => {
  try {
    const sns = new AWS.SNS({ apiVersion: '2010-03-31' });
    const response = await sns.publish(params).promise();
    consoleLogger.info({
      ref: 'SNS message sent',
      response,
    });
  } catch (err) {
    consoleLogger.error({
      ref: 'Error while sending SNS message',
      error: err.message,
    });
  }
};

module.exports = {
  sendSNSMessage,
};
