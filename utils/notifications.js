const { firebaseApp } = require('../configs/firebase');
const { consoleLogger } = require('../configs/logger');

const sendFirebaseMessage = async (messages) => {
  const bulkAmount = 500;
  try {
    for (let x = 0; x <= messages.length; x += bulkAmount) {
      // eslint-disable-next-line no-await-in-loop
      const response = await firebaseApp.messaging().sendAll(messages.slice(x, x + bulkAmount));
      consoleLogger.info({
        ref: 'Push Notification response',
        response,
      });
      consoleLogger.info(`Push notification sent to ${response.successCount} devices out of ${messages.slice(x, x + bulkAmount).length}`);
    }
  } catch (err) {
    consoleLogger.error({
      ref: 'Error while sending push notifications',
      error: err.message,
    });
  }
};

module.exports = {
  sendFirebaseMessage,
};
