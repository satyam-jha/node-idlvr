const Joi = require('@hapi/joi');
const { CustomError } = require('../configs/exceptions');
const strings = require('../configs/strings');

const randomString = (length) => {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const randomNumber = (length) => {
  let result = '';
  const characters = '0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const getCodeBasedError = (code, err) => {
  const newError = err;
  newError.actualMessage = err.actualMessage || err.message;
  if (err instanceof CustomError) {
    // pass
  } else if (err.details) {
    newError.message = err.details.map((x) => (x.message));
  } else if (err.name === 'MongoError') {
    if (err.code === 11000) {
      if ('email' in err.keyPattern) {
        newError.message = strings.EMAIL_ALREADY_REGISTERED;
      } else if ('shopSubdomain' in err.keyPattern) {
        newError.message = strings.SHOP_ALREADY_REGISTERED;
      }
    } else {
      newError.message = 'Server Error';
    }
  } else if (code === 404) {
    newError.message = strings.NOT_FOUND;
    newError.statusCode = 404;
  } else if (code === 401) {
    newError.message = strings.NO_AUTH;
    newError.statusCode = 401;
  } else {
    newError.message = 'Server Error';
    newError.statusCode = 500;
  }
  return newError;
};

const setPagination = async (ctx, next) => {
  const schema = Joi.object(
    {
      limit: Joi.number().min(0).required(),
      offset: Joi.number().min(0).required(),
    },
  ).unknown();
  ctx.request.query = await schema.validateAsync(ctx.request.query, { abortEarly: true });
  ctx.request.pagination = {
    skip: Number(ctx.request.query.offset),
    limit: Math.min(Number(ctx.request.query.limit), 100),
  };
  delete ctx.request.query.offset;
  delete ctx.request.query.limit;
  await next();
};

const setOrdering = async (ctx, next) => {
  const schema = Joi.object(
    {
      ordering: Joi.string(),
    },
  ).unknown();
  ctx.request.query = await schema.validateAsync(ctx.request.query, { abortEarly: true });
  ctx.request.ordering = ctx.request.query.ordering || '';
  delete ctx.request.query.ordering;
  await next();
};

const getResponsePagination = (ctx, count) => ({
  nextPage: count > (ctx.request.pagination.skip + ctx.request.pagination.limit),
  totalCount: count,
});

module.exports = {
  randomString,
  randomNumber,
  getCodeBasedError,
  setPagination,
  setOrdering,
  getResponsePagination,
};
