const jwt = require('jsonwebtoken');
const { CustomError } = require('../configs/exceptions');
const { NO_AUTH } = require('../configs/strings');

const getJWTToken = (payload, expiresIn = '1d') => jwt.sign(payload, process.env.JWT_SECRET, { expiresIn });

const resolveAuthorizationHeader = (authHeader) => {
  let token = authHeader;
  try {
    const parts = authHeader.trim().split(' ');

    if (parts.length === 2) {
      const scheme = parts[0];
      const credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        token = credentials;
      }
    }
    return token;
  } catch (err) {
    return token;
  }
};

const getJWTPayload = (token) => {
  const payload = jwt.verify(token, process.env.JWT_SECRET);
  return payload;
};

const isAuthentication = async (ctx, next) => {
  if (!ctx.state.user) {
    throw new CustomError(NO_AUTH, undefined, 401);
  }
  await next();
};

const isAdmin = async (ctx, next) => {
  if (!(ctx.state.user && ctx.state.isAdmin)) {
    throw new CustomError(NO_AUTH, undefined, 401);
  }
  await next();
};

module.exports = {
  getJWTToken,
  resolveAuthorizationHeader,
  isAuthentication,
  isAdmin,
  getJWTPayload,
};
