const AWS = require('aws-sdk');
const { consoleLogger } = require('../configs/logger');

const sendSQSMessage = async (params) => {
  try {
    const sqs = new AWS.SQS({ apiVersion: '2012-11-05' });
    const response = await sqs.sendMessage(params).promise();
    consoleLogger.info({
      ref: 'SQS message sent',
      response,
    });
  } catch (err) {
    consoleLogger.error({
      ref: 'Error while sending SQS message',
      error: err.message,
    });
  }
};

module.exports = {
  sendSQSMessage,
};
