const AWS = require('aws-sdk');
const { consoleLogger } = require('../configs/logger');

const sendSESMail = async (toAddresses, htmltext, subject) => {
  try {
    const params = {
      Destination: {
        CcAddresses: [],
        ToAddresses: toAddresses,
      },
      Message: {
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: htmltext,
          },
          Text: {
            Charset: 'UTF-8',
            Data: htmltext,
          },
        },
        Subject: {
          Charset: 'UTF-8',
          Data: subject,
        },
      },
      Source: process.env.SENDER_EMAIL_ADDRESS,
      ReplyToAddresses: [
        process.env.SENDER_EMAIL_ADDRESS,
      ],
    };
    const response = await new AWS.SES({ apiVersion: '2010-12-01' }).sendEmail(params).promise();
    consoleLogger.info({
      ref: 'Mail sending response',
      response,
    });
  } catch (err) {
    consoleLogger.error({
      ref: 'Error while sending email',
      error: err.message,
    });
  }
};

module.exports = {
  sendSESMail,
};
