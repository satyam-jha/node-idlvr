const jwt = require('jsonwebtoken');
const { consoleLogger } = require('../configs/logger');
const { resolveAuthorizationHeader } = require('./auth');
const { getCodeBasedError } = require('./base');
const User = require('../components/users/model');
const { CustomError } = require('../configs/exceptions');
const { NO_AUTH } = require('../configs/strings');

const requestLogger = async (ctx, next) => {
  const loggingData = {
    timestamp: (new Date()).toISOString(),
    ip: ctx.ip,
    url: ctx.request.url,
    query: ctx.request.query,
    body: ctx.request.body,
    headers: ctx.request.header,
    method: ctx.request.method,
  };
  await next();
  Object.assign(loggingData, {
    response: ctx.request.method === 'GET' ? undefined : ctx.body,
    error: ctx.exception ? ctx.exception.actualMessage || ctx.exception.message : null,
    errorStack: ctx.exception ? ctx.exception.stack : null,
    code: ctx.response.status,
    user: ctx.state.user || null,
    rt: ctx.state.rt,
  });
  if (loggingData.error) {
    consoleLogger.error(loggingData);
  } else {
    consoleLogger.info(loggingData);
  }
};

const jwtMiddleware = async (ctx, next) => {
  try {
    const authToken = ctx.header.authorization || ctx.cookies.get('authorization');
    if (authToken) {
      let payload = jwt.verify(
        resolveAuthorizationHeader(authToken),
        process.env.JWT_SECRET,
      );
      const validUser = await User.exists({idlvr_id: payload.user_id});
      if (!validUser) {
        // throw new CustomError(NO_AUTH, undefined, 401);
        payload.user_id = await (await User.create({idlvr_id:payload.user_id})).idlvr_id
      }
      ctx.state.user = payload.user_id;
    }
    await next();
  } catch (err) {
    consoleLogger.error(err);
    await next();
  }
};

const rtMiddleware = async (ctx, next) => {
  const start = Date.now();
  await next();
  const end = Date.now() - start;
  ctx.state.rt = `${end}ms`;
};

const responseMiddleware = async (ctx, next) => {
  const getResponseBody = () => ({
    status: ctx.exception ? 'error' : 'success',
    data: ctx.body ? ctx.body.data : null,
    // eslint-disable-next-line no-nested-ternary
    message: ctx.exception ? ctx.exception.message : (ctx.body ? ctx.body.message : null),
  });
  try {
    await next();
    if (ctx.status === 404) {
      throw new Error();
    }
    ctx.body = getResponseBody();
  } catch (err) {
    consoleLogger.error(err);
    ctx.exception = getCodeBasedError(ctx.status, err);
    ctx.body = getResponseBody();
    ctx.status = ctx.exception.statusCode || ctx.status;
  }
};

module.exports = {
  jwtMiddleware,
  requestLogger,
  rtMiddleware,
  responseMiddleware,
};
