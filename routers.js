const Router = require('@koa/router');
const userRouter = require('./components/users/router');
const roomRouter = require('./components/chatroom/router');
const chatRouter = require('./components/chat/router');
const historyRouter = require('./components/history/router');


const routerV1 = new Router();

routerV1.use('/users', userRouter.routes());
routerV1.use('/room', roomRouter.routes());
routerV1.use('/history', historyRouter.routes());
routerV1.use('/chat', chatRouter.routes());

module.exports = {
  routerV1,
};