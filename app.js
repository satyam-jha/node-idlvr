const cors = require("@koa/cors");
const Koa = require('koa');
const {
    jwtMiddleware, rtMiddleware, responseMiddleware, requestLogger,
  } = require('./utils/middlewares');
const { routerV1 } = require('./routers');
const bodyParser = require('koa-bodyparser');
const Router = require('@koa/router');
const socket = require('socket.io');

  
require("dotenv").config();




const app = new Koa();
app.use(cors());

app.use(bodyParser({
  formLimit: '10mb',
  jsonLimit: '10mb',
}));

app.use(jwtMiddleware);

app.use(rtMiddleware);

app.use(responseMiddleware);

app.use(requestLogger);

const router = new Router();
router.use('/v1', routerV1.routes());

app.use(router.routes());

const io = socket(app)

global.onlineUsers = new Map();

io.on('connection', (socket) =>{
  global.chatSocket = socket;
  socket.on('add-user', (userId) =>{
    onlineUsers.set(userId, socket.id);
  });
 
  socket.on('send-msg', (data) =>{
    const senduserSocket = onlineUsers.get(data.to);
    if(senduserSocket){
      socket.to(senduserSocket).emit("msg-recive", data.message);
    }
  });

})

module.exports = app;