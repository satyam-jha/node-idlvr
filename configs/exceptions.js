class CustomError extends Error {
  constructor(message, actualMessage = null, statusCode = 200) {
    super(message);
    this.actualMessage = actualMessage || message;
    this.message = message;
    this.name = 'CustomError';
    this.statusCode = statusCode;
  }
}

module.exports = {
  CustomError,
};
