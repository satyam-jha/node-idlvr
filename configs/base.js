const multer = require('@koa/multer');
const uuid = require('uuid');

const storage = multer.diskStorage({
  destination: (_, __, cb) => {
    cb(null, '/tmp/');
  },
  filename: (_, file, cb) => {
    cb(null, `${uuid.v4().replace(/-/g, '')}.${file.originalname.split('.')[file.originalname.split('.').length - 1]}`);
  },
});

module.exports = {
  storage,
};
