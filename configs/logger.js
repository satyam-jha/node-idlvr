const winston = require('winston');

const consoleLogger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.Console(),
  ],
});

module.exports = {
  consoleLogger,
};
